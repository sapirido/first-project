// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAbujbkt2OzanKTfujVNonkWXBW9ORfNoc",
    authDomain: "first-project-jce.firebaseapp.com",
    databaseURL: "https://first-project-jce.firebaseio.com",
    projectId: "first-project-jce",
    storageBucket: "first-project-jce.appspot.com",
    messagingSenderId: "422826231995",
    appId: "1:422826231995:web:1c47509f79c7775049019e",
    measurementId: "G-5ZT2BY3SG7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
