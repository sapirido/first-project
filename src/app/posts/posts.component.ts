import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';


@Component({
  selector: 'posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts$;
  panelOpenState = false;
  hasError = false;
  constructor(private postsService:PostsService) { }

  ngOnInit(): void {
    this.posts$ = this.postsService.getPosts();
    this.posts$.subscribe(
      data => {}
      ,
      error =>{
        this.hasError = true;
      }
    )
  }

}
