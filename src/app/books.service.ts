import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Book } from './interfaces/book';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  books = [{title:'Alice in Wonderland', author:'Lewis Carrol', summary:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."},
  {title:'War and Peace', author:'Leo Tolstoy',summary:"Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source."},
  {title:'The Magic Mountain', author:'Thomas Mann',summary:"There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."}];
  updateBook: any;
  
  constructor(private db:AngularFirestore) { }

  public addBooks(){
    setInterval( ()=>this.books.push({title:'A new one', author:'New author', summary:'Short summary'}),2000);
  }

  // public getBooks(){
  //   const booksObservable = new Observable(observer => {
  //    setInterval(()=>observer.next(this.books),500)
  //   });
  //   return booksObservable;
  // }

  bookCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  // public getBooks(userId){
  //   this.bookCollection = this.db.collection(`users/${userId}/books`);
  //   return this.bookCollection.snapshotChanges().pipe(map(
  //     collection => collection.map(
  //     document => {
  //     const data = document.payload.doc.data();
  //     data.id = document.payload.doc.id;
  //     return data;
  //   })))
  // }

  public getBooks(userId,startAfter,type){
    if(type === 'next'){
      this.bookCollection = this.db.collection(`users/${userId}/books`,
      ref => ref.orderBy('title','asc').limit(4).startAfter(startAfter));
    }
    if(type === 'prev'){
      this.bookCollection = this.db.collection(`users/${userId}/books`,
     ref => ref.orderBy('title','asc').limitToLast(4).endBefore(startAfter));
    }
    return this.bookCollection.snapshotChanges()
  }

  deleteBook(userid:string, id:string){
    this.db.doc(`users/${userid}/books/${id}`).delete();
  }

  addBook(userId:string,title:string, author:string){
    const book = {title:title, author:author};
    this.userCollection.doc(userId).collection('books').add(book);
  }

  update(userId:string,id:string,title:string,author:string){
    this.db.doc(`users/${userId}/books/${id}`).update(
      {
      title:title,
      author:author
      }
    );
  }
  

  // getBooks(userId):Observable<any[]>{

  // }

}
