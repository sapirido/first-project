import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import {FormControl, Validators} from '@angular/forms';
@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email:string;
  password:string;
  confirmedPassword:string;
  emailErrorMessage:string;
  passwordsError:string;
  
    onSubmit(){
    if(this.password === this.confirmedPassword){
      this.auth.register(this.email,this.password)
      .then(res => {
        this.router.navigate(['/login']);
      })
      .catch(err =>{
        this.emailErrorMessage = err.message;
      })
    }else{
     this.passwordsError = 'the passwords did not match';
    }
  }

  clearError(){
    this.emailErrorMessage = '';
    this.passwordsError = '';
  }

  constructor(private auth:AuthService,private router:Router) { }

  ngOnInit(): void {
  }

}
