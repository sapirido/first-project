import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;
  ErrorMessage:string;


  onSubmit(){
    this.auth.login(this.email,this.password).then(res => {
    this.router.navigate(['/books']);
    })
    .catch(err => {this.ErrorMessage = err.message; 
    })
    }

    clearError(){
      this.ErrorMessage = '';
    }

  constructor(private auth:AuthService,private router:Router) { }

  ngOnInit(): void {
  }

}
