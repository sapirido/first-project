import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { PostRaw } from './interfaces/post-raw';
import { throwError, Observable } from 'rxjs';
import { Post } from './interfaces/post';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private URL = "https://jsonplaceholder.typicode.com/posts/";

  constructor(private http:HttpClient) { }

  getPosts():Observable<Post[]>{
  //  getPosts():Observable<Post>{
    //return this.http.get<Post>(this.URL);
    return this.http.get<PostRaw[]>(this.URL).pipe(
      map(posts => posts.map(post => this.transformPostsData(post))),
      catchError(this.handleError)
    )
  }

  private handleError(res:HttpErrorResponse){
    return throwError(res.error || 'Server error');
  }

  private transformPostsData(post:PostRaw):Post{  
    return {
   id:post.id,
   title:post.title,
   body:post.body    
   }
  }
}
