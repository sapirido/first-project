import { ImageService } from './../image.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClassifyService } from '../classify.service';

@Component({
  selector: 'classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})

export class ClassifyComponent implements OnInit  {
  favoriteOption: string;
  options: string[] = ['BBC', 'CNN', 'NBC'];
  text:string;
  category:string = 'No Category';
  categoryImage:string;

  classify(){
    this.classifyService.classify(this.text).subscribe(
      res => {
        console.log(res);
        this.category = this.classifyService.categories[res];
        this.categoryImage = this.imageService.images[res];
      }
    )
  }

  constructor(private route:ActivatedRoute, 
              private classifyService:ClassifyService,
              private imageService:ImageService) {}

  ngOnInit(): void {
    const isExist = this.options.some(option => option === this.route.snapshot.params.bbc);
    // .toLowerCase()
    if(isExist){
      this.favoriteOption = this.route.snapshot.params.bbc.toUpperCase();
    }else{
      alert("The value from the URL is illegal")
    }
  }
}

