import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { BooksService } from '../books.service';
import { Book } from '../interfaces/book';


@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  books:Book[];
  books$;
  userId:string;
  editstate = [];
  addBookFormOpen = false;
  panelOpenState = false;
  lastDocumentArrived;
  prevRef;

  constructor(private booksService:BooksService,
              public authService : AuthService) { }


  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.books$ = this.booksService.getBooks(this.userId,null,'next');
        this.books$.subscribe(
          docs => {
            this.lastDocumentArrived = docs[docs.length-1].payload.doc;
            this.prevRef = null;
            this.books = [];
            for(let document of docs){
              const book:Book = document.payload.doc.data();
              book.id = document.payload.doc.id;
              this.books.push(book);
            }
          }
        )
      }
    )
  // this.booksService.addBooks();
  // this.books$ = this.booksService.getBooks();
  //this.books$.subscribe(books => this.books = books);
  }

  deleteBook(id:string){
    this.booksService.deleteBook(this.userId,id);
  }

  update(book:Book){
    this.booksService.update(this.userId,book.id,book.title,book.author)
  }

  add(book:Book){
    this.booksService.addBook(this.userId,book.title,book.author)
  }

  nextPage(){
        this.books$ = this.booksService.getBooks(this.userId,this.lastDocumentArrived,'next');
        this.books$.subscribe(
          docs => {
            this.prevRef = docs[0].payload.doc;
            this.lastDocumentArrived = docs[docs.length-1].payload.doc;
        
            this.books = [];
            for(let document of docs){
              const book:Book = document.payload.doc.data();
              book.id = document.payload.doc.id;
              this.books.push(book);
            }
          }
        )
      }

        prevPage(){
        this.books$ = this.booksService.getBooks(this.userId,this.prevRef,'prev');
        this.books$.subscribe(
          docs => {
            this.books = [];
            this.lastDocumentArrived = docs[docs.length-1].payload.doc;
            this.prevRef = docs[0].payload.doc;
            for(let document of docs){
              const book:Book = document.payload.doc.data();
              book.id = document.payload.doc.id;
              this.books.push(book);
            }
          }
        )
      }
}
