import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  private path:string = 'https://firebasestorage.googleapis.com/v0/b/first-project-jce.appspot.com/o/';
  public images:string[] = [];
  
  constructor() {
    this.images[0] = this.path + 'biz.JPG'+'?alt=media&token=e72e43a6-958a-4209-a3cf-40b65087f6d6';
    this.images[1] = this.path + 'entermnt.JPG'+'?alt=media&token=4e91525a-5411-43d5-97cd-79c125844747';
    this.images[2] = this.path + 'politics-icon.png'+'?alt=media&token=b1f7f943-2914-4db4-a58d-84e0743409d4';
    this.images[3] = this.path + 'sport.JPG'+'?alt=media&token=6370f423-a365-48c4-9aef-ac25d32929da';
    this.images[4] = this.path + 'tech.JPG'+'?alt=media&token=10386f32-eccb-4ec6-8d47-4e716a2e94ef';
   }
}
